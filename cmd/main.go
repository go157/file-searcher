package main

import (
	"gitlab.com/ffsaschagff/file-searcher/internal/configcli"
)

func main() {
	config := configcli.GetConfig()
	config.Command.Do()
}
