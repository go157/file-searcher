package configcli

import (
	"fmt"
	"os"
	"sort"

	"gitlab.com/ffsaschagff/file-searcher/internal/searcher"
)

// Config - description of command to run.
type Config struct {
	Command Command
}

// for size to string transform.
const (
	sizeKb = 1024
	sizeMb = 1048576
	sizeGb = 1073741824
	sizeTb = 1099511627776
)

// Command of cli - when you print it and press enter it will do Do().
type Command interface {
	Do()
}

// Help - command for help.
type Help struct {
	command string
}

// Search - command for search.
type Search struct {
	path string
}

// GetConfig - transform os.Args to Config.
func GetConfig() Config {
	config := Config{}
	args := os.Args
	if len(args) == 1 {
		config.Command = Help{}

		return config
	}

	switch args[1] {
	case "search":
		path := "C:\\"
		if len(args) > 2 {
			path = args[2]
		}
		config.Command = Search{
			path: path,
		}
	default:
		command := ""
		if len(args) > 2 {
			command = args[2]
		}
		config.Command = Help{
			command: command,
		}
	}

	return config
}

// Do - for help.
func (help Help) Do() {
	switch help.command {
	case "search":
		fmt.Println(`Поиск по файлу. Параметры:
	search <path>`)
	default:
		fmt.Println(`Поисковик по файлам, помогает найти объекты, которые часто теряются
	Комманды:
		help <command> - справка о команде
		search <path> - поиск по пути`)
	}
}

// Do - for search.
func (search Search) Do() {
	objects := searcher.FindObjects(search.path)
	fmt.Println("Name\tType\tSize")
	comparator := func(object1index int, object2index int) bool {
		return objects[object1index].Size > objects[object2index].Size
	}
	sort.Slice(objects, comparator)
	for _, object := range objects {
		fmt.Printf("%s\t%s\t%s\n",
			object.Title,
			object.Type.String(),
			sizeToString(object.Size))
	}
}

func sizeToString(size int64) string {
	switch {
	case size < sizeKb:
		return fmt.Sprintf("%d байт", size)
	case size < sizeMb:
		return fmt.Sprintf("%d Кб", size/sizeKb)
	case size < sizeGb:
		return fmt.Sprintf("%d Мб", size/sizeMb)
	case size < sizeTb:
		return fmt.Sprintf("%d Гб", size/sizeGb)
	}

	return "Очень много"
}
