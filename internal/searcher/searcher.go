package searcher

import (
	"io/fs"
	"log"
	"os"
	"regexp"
	"syscall"
)

// FSEntity - description of big object.
type FSEntity interface {
	Files() []string
	Root() string
	Type() EntityType
}

// EntityType - enum for types.
type EntityType byte

const (
	TypeOnecBase EntityType = iota
	TypeRepository
)

// FSEntityInfo - big object info.
type FSEntityInfo struct {
	Title string
	Size  int64
	Type  EntityType
}

// Repository - git repository.
type Repository struct {
	Path string
}

// OnecBase - 1C file base.
type OnecBase struct {
	Path string
}

// FindObjects - look for big files.
func FindObjects(path string) []FSEntityInfo {
	objects := checkDirectory(path)
	infos := make([]FSEntityInfo, len(objects))
	for index, object := range objects {
		info := FSEntityInfo{
			Title: object.Root(),
			Type:  object.Type(),
			Size:  getSize(object),
		}
		infos[index] = info
	}

	return infos
}

func checkDirectory(path string) []FSEntity {
	result := []FSEntity{}

	files, err := os.ReadDir(path)
	if err != nil && !isAccessErr(err) {
		log.Println(err)
	}

	for _, file := range files {
		if err != nil {
			log.Println(err)
		}

		if file.Name() == ".git" && file.IsDir() {
			repository := Repository{
				Path: path,
			}
			result = append(result, repository)
		}

		if file.Name() == "1Cv8.1CD" && !file.IsDir() {
			onec := OnecBase{
				Path: path,
			}
			result = append(result, onec)
		}

		if file.IsDir() {
			result = append(result, checkDirectory(concatPath(path, file.Name()))...)
		}
	}

	return result
}

func (rep Repository) Files() []string {
	return []string{rep.Path}
}

func (rep Repository) Root() string {
	return rep.Path
}

func (rep Repository) Type() EntityType {
	return TypeRepository
}

func (base OnecBase) Files() []string {
	result := []string{}

	files, err := os.ReadDir(base.Path)
	if err != nil {
		log.Println(err)
	}

	matcher := regexp.MustCompile(`^(1Cv8FTxt|1Cv8Log|1Cv8JobScheduler|1CHelpIndex|1Cv8\.1CD|.+\.cfl)$`)
	for _, file := range files {
		filename := file.Name()
		if matcher.Match([]byte(filename)) {
			result = append(result, concatPath(base.Path, filename))
		}
	}

	return result
}

func (base OnecBase) Root() string {
	return base.Path
}

func (base OnecBase) Type() EntityType {
	return TypeOnecBase
}

func isAccessErr(err error) bool {
	pathErr, ok := err.(*fs.PathError)
	if !ok {
		return false
	}
	sysErr, ok := pathErr.Err.(syscall.Errno)
	if !ok {
		return false
	}

	return sysErr == syscall.ERROR_ACCESS_DENIED
}

func concatPath(paths ...string) string {
	path := ""
	for _, subdir := range paths {
		if path == "" || path[len(path)-1:] == "\\" {
			path += subdir
		} else {
			path += "\\" + subdir
		}
	}

	return path
}

func (entityType EntityType) String() string {
	switch entityType {
	case TypeOnecBase:
		return "База 1С"
	case TypeRepository:
		return "Репозиторий"
	}

	return "Неопределено"
}

func getSize(object FSEntity) int64 {
	var size int64
	for _, path := range object.Files() {
		fileInfo, err := os.Stat(path)
		if err != nil {
			continue
		}

		if fileInfo.IsDir() {
			size += folderSize(path)
		} else {
			size += fileInfo.Size()
		}
	}

	return size
}

func folderSize(path string) int64 {
	var size int64
	files, err := os.ReadDir(path)
	if err != nil {
		return size
	}
	for _, file := range files {
		if file.IsDir() {
			size += folderSize(concatPath(path, file.Name()))
		} else {
			info, err := file.Info()
			if err != nil {
				continue
			}
			size += info.Size()
		}
	}

	return size
}
